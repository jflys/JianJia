package poetry.jianjia.http;

/**
 * @author 裴云飞
 * @date 2021/4/26
 */

public @interface PartMap {

    String encoding() default "binary";
}
